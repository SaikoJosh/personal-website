const SIZINGS = {
	'w-screen': '100vw',
	'h-screen': '100vh',
	'500': `500px`,
	'60pc': `60%`,
	'main': `60rem`,
};

module.exports = {
	content: ['./src/**/*.{js,jsx,ts,tsx}'],
	theme: {
		extend: {
			fontFamily: {
				logo: ['Parisienne'],
			},
			flex: {
				2: '2 2 0%',
			},
			inset: {
				'5pc': '5%',
				'10pc': '10%',
				'15pc': '15%',
				'20pc': '20%',
				'25pc': '25%',
				'30pc': '30%',
				'35pc': '35%',
				'40pc': '40%',
				'45pc': '45%',
				'50pc': '50%',
			},
			minWidth: {
				...SIZINGS,
			},
			minHeight: {
				...SIZINGS,
			},
			maxWidth: {
				...SIZINGS,
			},
			maxHeight: {
				...SIZINGS,
			},
			width: {
				...SIZINGS,
			},
			height: {
				...SIZINGS,
			},
			backgroundImage: {
				'img-main': "url('/images/background2.jpg')",
				'img-me': "url('/images/me2.jpg')",
			},
			textUnderlineOffset: { 16: '16px' },
		},
	},
	plugins: [require('@tailwindcss/typography'), require('@tailwindcss/line-clamp')],
};
