import * as Sentry from '@sentry/gatsby';
import { BrowserTracing } from '@sentry/tracing';

Sentry.init({
	dsn: 'https://dbc9aac5506a4538b5c50ae3bb88a386@o1348009.ingest.sentry.io/6627069',
	sampleRate: 1.0,
	integrations: [new BrowserTracing()],
});
