require(`dotenv`).config({
	path: `.env.${process.env.NODE_ENV}`,
});

const SITE_DOMAIN = `joshuacole.me`;

module.exports = {
	siteMetadata: {
		siteDomain: SITE_DOMAIN,
		siteUrl: `https://www.${SITE_DOMAIN}`,
		title: `Joshua Cole`,
		description: `Expert technical leader, software engineer, and speaker based in London, UK.`,
		author: {
			firstName: `Joshua`,
			lastName: `Cole`,
		},
		socialLinks: {
			linkedIn: `https://www.linkedin.com/in/joshcoleuk`,
			twitter: `https://twitter.com/saikojosh`,
			email: `hello@${SITE_DOMAIN}`,
		},
	},
	trailingSlash: `never`,
	plugins: [
		{
			resolve: `gatsby-plugin-sass`,
			options: {
				postCssPlugins: [
					require(`tailwindcss`),
					require(`./tailwind.config.js`), // Optional: Load custom Tailwind CSS configuration
				],
			},
		},
		`gatsby-plugin-image`,
		{
			resolve: `gatsby-plugin-google-analytics`,
			options: {
				trackingId: process.env.GOOGLE_ANALYTICS_ID,
				head: false,
				anonymize: false,
				respectDNT: false,
				pageTransitionDelay: 0,
			},
		},
		`gatsby-plugin-react-helmet`,
		`gatsby-plugin-sitemap`,
		`gatsby-plugin-sharp`,
		`gatsby-transformer-sharp`,
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `./src/images/`,
			},
			__key: `images`,
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `pages`,
				path: `./src/pages/`,
			},
			__key: `pages`,
		},
		{
			resolve: `gatsby-transformer-remark`,
			options: {
				footnotes: true,
				gfm: true,
			},
			plugins: [
				{
					resolve: `gatsby-remark-autolink-headers`, // Must be before Prism.
					options: {
						enableCustomId: true,
						isIconAfterHeader: false,
					},
				},
				{
					resolve: `gatsby-remark-prismjs`,
					options: {
						classPrefix: `language-`,
						inlineCodeMarker: `›`,
						showLineNumbers: true,
						noInlineHighlight: false,
						prompt: {
							user: `root`,
							host: `localhost`,
							global: false,
						},
					},
				},
				{
					resolve: `gatsby-remark-external-links`,
					options: {
						target: `_blank`,
						rel: `noreferrer noopener`,
					},
				},
			],
		},
		{
			resolve: `gatsby-plugin-transition-link`,
		},
		{
			resolve: `gatsby-plugin-netlify`,
		},
		{
			resolve: `gatsby-plugin-react-svg`,
			options: {
				rule: {
					include: /images/,
				},
			},
		},
		{
			resolve: `gatsby-source-notion-api`,
			options: {
				token: process.env.NOTION_KEY,
				databaseId: process.env.NOTION_DATABASE_ID,
				propsToFrontmatter: true,
				lowerTitleLevel: true,
			},
		},
		{
			resolve: `gatsby-plugin-sitemap`,
		},
		{
			resolve: `gatsby-plugin-robots-txt`,
			options: {
				policy: [{ userAgent: `*`, allow: `/` }],
			},
		},
		{
			resolve: '@sentry/gatsby',
		},
	],
};
